package pzn_go_http_router

import (
	"fmt"
	"github.com/julienschmidt/httprouter"
	"github.com/stretchr/testify/assert"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"
)

func IndexParams(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	paramId := ps.ByName("id")
	fmt.Fprintf(w, "that's me %s", paramId)
}

func TestParamsServer(t *testing.T) {
	router := httprouter.New()

	router.GET("/product/:id", IndexParams)

	server := http.Server{
		Addr:    "localhost:8080",
		Handler: router,
	}

	err := server.ListenAndServe()
	if err != nil {
		panic(err)
	}
}

func TestParams(t *testing.T) {
	router := httprouter.New()
	router.GET("/product/:id", func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		paramId := ps.ByName("id")
		fmt.Fprintf(w, "that's product to %s", paramId)
	})

	request := httptest.NewRequest(http.MethodGet, "http://localhost:8080/product/1", nil)
	recorder := httptest.NewRecorder()

	router.ServeHTTP(recorder, request)

	response := recorder.Result()
	body, _ := io.ReadAll(response.Body)
	assert.Equalf(t, "that's product to 1", string(body), "success")
}
