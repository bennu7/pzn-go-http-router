package pzn_go_http_router

import (
	"fmt"
	"github.com/julienschmidt/httprouter"
	"github.com/stretchr/testify/assert"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"
)

func Index(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	paramName := ps.ByName("name")
	fmt.Fprintf(w, "that's me %s", paramName)
}

func TestRouterServer(t *testing.T) {
	router := httprouter.New()

	router.GET("/:name", Index)

	server := http.Server{
		Addr:    "localhost:8080",
		Handler: router,
	}

	err := server.ListenAndServe()
	if err != nil {
		panic(err)
	}
}

func TestRouter(t *testing.T) {
	request := httptest.NewRequest(http.MethodGet, "http://localhost:8080", nil)
	recorder := httptest.NewRecorder()
	params := httprouter.Params{{Key: "name", Value: "bennu"}}

	Index(recorder, request, params)

	response := recorder.Result()
	body, _ := io.ReadAll(response.Body)
	assert.Equalf(t, "that's me bennu", string(body), "success")
}
