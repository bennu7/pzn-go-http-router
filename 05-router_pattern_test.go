package pzn_go_http_router

import (
	"fmt"
	"github.com/julienschmidt/httprouter"
	"github.com/stretchr/testify/assert"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"
)

// *02 Catch All Parameter, bisa menangkap semua parameter berapapun jumlahnya
func TestParamsCatchAllParameter(t *testing.T) {
	router := httprouter.New()
	router.GET("/images/*image", func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		catchParams := "Image " + ps.ByName("image")
		fmt.Fprintf(w, catchParams)
	})

	request := httptest.NewRequest(http.MethodGet, "http://localhost:8080/images/small/profile.png", nil)
	recorder := httptest.NewRecorder()

	router.ServeHTTP(recorder, request)

	response := recorder.Result()
	body, _ := io.ReadAll(response.Body)
	assert.Equalf(t, "Image /small/profile.png", string(body), "ini harus matching")
}

// *01 Named Parameter
func TestParamsNamedRouter(t *testing.T) {
	router := httprouter.New()
	router.GET("/product/:id/items/:secondId", func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		moreParams := "product from " + ps.ByName("id") + " to " + ps.ByName("secondId")
		fmt.Fprintf(w, moreParams)
	})

	request := httptest.NewRequest(http.MethodGet, "http://localhost:8080/product/1/items/3", nil)
	recorder := httptest.NewRecorder()

	router.ServeHTTP(recorder, request)

	response := recorder.Result()
	body, _ := io.ReadAll(response.Body)
	assert.Equalf(t, "product from 1 to 3", string(body), "success")
}
