package pzn_go_http_router

import (
	"fmt"
	"github.com/julienschmidt/httprouter"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"
)

type LogMiddleware struct {
	http.Handler
}

func (logMiddleware *LogMiddleware) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	fmt.Println("receive request")
	logMiddleware.Handler.ServeHTTP(w, r)
	fmt.Println("after middleware")
}

func TestMiddlewareServer(t *testing.T) {
	router := httprouter.New()

	router.GET("/", func(writer http.ResponseWriter, request *http.Request, params httprouter.Params) {
		fmt.Println("OK")
		fmt.Fprint(writer, "OK!")
	})

	//logMiddleware := &LogMiddleware{
	//	Handler: router,
	//}
	logMiddleware := new(LogMiddleware)
	logMiddleware.Handler = router

	server := http.Server{
		Addr:    "localhost:8080",
		Handler: logMiddleware,
	}

	err := server.ListenAndServe()
	if err != nil {
		panic(err)
	}
}

func TestMiddlewareTesting(t *testing.T) {
	router := httprouter.New()
	router.GET("/", func(writer http.ResponseWriter, request *http.Request, params httprouter.Params) {
		fmt.Fprint(writer, "OK!")
	})

	middleware := &LogMiddleware{
		Handler: router,
	}

	request := httptest.NewRequest(http.MethodGet, "http://localhost:8080/", nil)
	recorder := httptest.NewRecorder()

	// !disini manfaatkan middleware untuk ServeHTTP bukan router
	middleware.ServeHTTP(recorder, request)

	response := recorder.Result()
	body, _ := io.ReadAll(response.Body)
	fmt.Println(string(body))
	//assert.Equalf(t, "method engga cocok!", string(body), "Middleware")
}
