package pzn_go_http_router

import (
	"fmt"
	"github.com/julienschmidt/httprouter"
	"github.com/stretchr/testify/assert"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestMethodNotAllowedHandlerServer(t *testing.T) {
	router := httprouter.New()

	// *untuk Method Not Allowed manfaatkan MethodNotAllowed() dari router dengan mengisi httpHandler, jangan lupa mengirim status code nya 405
	// *Method Not Allowed disini merupakan method yang tidak ada di routing, misal endpoint / hanya get saja, yang lain akan menghasilkan error
	router.MethodNotAllowed = http.HandlerFunc(func(writer http.ResponseWriter, request *http.Request) {
		writer.WriteHeader(http.StatusMethodNotAllowed)
		fmt.Fprint(writer, "method engga cocok!")
	})

	router.NotFound = http.HandlerFunc(func(writer http.ResponseWriter, request *http.Request) {
		fmt.Fprint(writer, "routing not found")
	})

	router.GET("/", func(writer http.ResponseWriter, request *http.Request, params httprouter.Params) {
		fmt.Fprint(writer, "OK!")
	})

	server := http.Server{
		Addr:    "localhost:8080",
		Handler: router,
	}

	err := server.ListenAndServe()
	if err != nil {
		panic(err)
	}
}

func TestMethodNotAllowedHandler(t *testing.T) {
	router := httprouter.New()
	//	!gunakan MethodNotAllowed
	router.MethodNotAllowed = http.HandlerFunc(func(writer http.ResponseWriter, request *http.Request) {
		writer.WriteHeader(http.StatusMethodNotAllowed)
		fmt.Fprint(writer, "method engga cocok!")
	})

	router.GET("/", func(writer http.ResponseWriter, request *http.Request, params httprouter.Params) {
		fmt.Fprint(writer, "OK!")
	})

	request := httptest.NewRequest(http.MethodPost, "http://localhost:8080/", nil)
	recorder := httptest.NewRecorder()

	router.ServeHTTP(recorder, request)

	response := recorder.Result()
	body, _ := io.ReadAll(response.Body)
	assert.Equalf(t, "method engga cocok!", string(body), "routing")
}
