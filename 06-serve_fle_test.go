package pzn_go_http_router

import (
	"embed"
	_ "embed"
	"github.com/julienschmidt/httprouter"
	"github.com/stretchr/testify/assert"
	"io"
	"io/fs"
	"net/http"
	"net/http/httptest"
	"testing"
)

//go:embed resources/*
var resources embed.FS

func TestServeFile(t *testing.T) {
	router := httprouter.New()
	// *parameter ke dua simpan nama foldernya
	directory, _ := fs.Sub(resources, "resources")

	// *manfaatkan catch all parameter, wajib menggunakan nama *filepath, jika tidak = error
	router.ServeFiles("/files/*filepath", http.FS(directory))

	request := httptest.NewRequest(http.MethodGet, "http://localhost:8080/files/file.txt", nil)
	recorder := httptest.NewRecorder()

	router.ServeHTTP(recorder, request)

	response := recorder.Result()
	body, _ := io.ReadAll(response.Body)
	assert.Equalf(t, "woii", string(body), "get a file serve file")
}
